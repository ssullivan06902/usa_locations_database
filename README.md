<h1>USA Locations Database README</h1>

<b>Example</b><br>
sqlite3 ./locations.db<br>
.schema location<br>
.tables<br>
select * from location;<br>

<p>
US_States.csv<br>
Simply an enumeration of all fifty states. The id increments in the order of statehood.
</p>

<p>
US_Counties.csv<br>
All counties for each state in the United States. I did not include the U.S. territories.
</p>


<p>
I'll add some python examples soon.
</p>


<p>
I found some additional data sources. I'll try to normalize the tables and make the data 
better for use. They can be found in the CSV files directory.
</p>
